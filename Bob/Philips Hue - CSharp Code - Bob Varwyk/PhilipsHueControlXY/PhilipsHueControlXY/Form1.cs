﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Windows.Forms;

namespace PhilipsHueControlXY
{
    public partial class SetHueXy : Form
    {

        ColorConverter colorValue = new ColorConverter();

        public SetHueXy()
        {
            InitializeComponent();

            HueConnect bridge = new HueConnect();

            bridge.findBridgeIP();
            IpLbl.Text = HueConnect.BridgeIP;   

        }

        public float[] convertColor(Color color)
        {

            float[] color_final;

            float red = Convert.ToInt32(color.R) / 255f;
            float green = Convert.ToInt32(color.G) / 255f;
            float blue = Convert.ToInt32(color.B) / 255f;

            red = (red > 0.04045f) ? (float)Math.Pow((red + 0.055f) / (1.0f + 0.055f), 2.4f) : (red / 12.92f);
            green = (green > 0.04045f) ? (float)Math.Pow((green + 0.055f) / (1.0f + 0.055f), 2.4f) : (green / 12.92f);
            blue = (blue > 0.04045f) ? (float)Math.Pow((blue + 0.055f) / (1.0f + 0.055f), 2.4f) : (blue / 12.92f);

            float X = red * 0.664511f + green * 0.154324f + blue * 0.162028f;
            float Y = red * 0.283881f + green * 0.668433f + blue * 0.047685f;
            float Z = red * 0.000088f + green * 0.072310f + blue * 0.986039f;

            float x = X / (X + Y + Z);
            float y = Y / (X + Y + Z);

            string outputX = x.ToString().Replace(',', '.');
            string outputY = y.ToString().Replace(',', '.');

            color_final = new float[] { x, y };
            return color_final;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            ColorJson dataFormer = new ColorJson(convertColor(Color.Green), 10);
            string data = JsonConvert.SerializeObject(dataFormer);

            ColorSender Lights = new ColorSender();
            Lights.putRequestToBridge(string.Format(ColorSender.ControlLightUrlTemplate, HueConnect.BridgeIP, "srBM4Wmu7FgO4qHx0o6MNdibKXtOrpZurFioT17a", "lights", 1, "state"), data);
        }
    }
}
