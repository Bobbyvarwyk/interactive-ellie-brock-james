﻿namespace PhilipsHueBasicController
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtTransTime = new System.Windows.Forms.TextBox();
            this.TxtHue = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBrightness = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtSat = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSend = new System.Windows.Forms.Button();
            this.IpLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Transition time";
            // 
            // txtTransTime
            // 
            this.txtTransTime.Location = new System.Drawing.Point(16, 30);
            this.txtTransTime.Name = "txtTransTime";
            this.txtTransTime.Size = new System.Drawing.Size(93, 20);
            this.txtTransTime.TabIndex = 1;
            // 
            // TxtHue
            // 
            this.TxtHue.Location = new System.Drawing.Point(16, 82);
            this.TxtHue.Name = "TxtHue";
            this.TxtHue.Size = new System.Drawing.Size(93, 20);
            this.TxtHue.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Hue";
            // 
            // txtBrightness
            // 
            this.txtBrightness.Location = new System.Drawing.Point(16, 135);
            this.txtBrightness.Name = "txtBrightness";
            this.txtBrightness.Size = new System.Drawing.Size(93, 20);
            this.txtBrightness.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Brightness";
            // 
            // TxtSat
            // 
            this.TxtSat.Location = new System.Drawing.Point(16, 186);
            this.TxtSat.Name = "TxtSat";
            this.TxtSat.Size = new System.Drawing.Size(93, 20);
            this.TxtSat.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Saturation";
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(16, 224);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(93, 23);
            this.btnSend.TabIndex = 8;
            this.btnSend.Text = "Change Color";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.button1_Click);
            // 
            // IpLbl
            // 
            this.IpLbl.AutoSize = true;
            this.IpLbl.Location = new System.Drawing.Point(16, 271);
            this.IpLbl.Name = "IpLbl";
            this.IpLbl.Size = new System.Drawing.Size(23, 13);
            this.IpLbl.TabIndex = 9;
            this.IpLbl.Text = "IP :";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(201, 330);
            this.Controls.Add(this.IpLbl);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.TxtSat);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBrightness);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtHue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTransTime);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTransTime;
        private System.Windows.Forms.TextBox TxtHue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBrightness;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtSat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label IpLbl;
    }
}

