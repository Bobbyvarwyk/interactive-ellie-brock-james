﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;

namespace PhilipsHueBasicController
{
    public partial class JsonParser
    {
        [JsonProperty("state")]
        public bool onoff { get; set; }
        [JsonProperty("transitiontime")]
        public int transitiontime { get; set; }
        [JsonProperty("hue")]
        public int hue { get; set; }
        [JsonProperty("sat")]
        public int saturation { get; set; }
        [JsonProperty("bri")]
        public int brightness { get; set; }

        public JsonParser(bool state, int transition, int hueColor, int sat, int bri)
        {
            onoff = state;
            transitiontime = transition;
            hue = hueColor;
            saturation = sat;
            brightness = bri;
        }
    }
}
