﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Windows.Forms;

namespace PhilipsHueBasicController
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            HueConnect bridge = new HueConnect();

            bridge.findBridgeIP();
            IpLbl.Text = HueConnect.BridgeIP;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            JsonParser JsonData = new JsonParser(true, 10, 24500, 255, 255);
            string data = JsonConvert.SerializeObject(JsonData);

            ColorSender controls = new ColorSender();
            controls.putRequestToBridge(string.Format(ColorSender.ControlLightUrlTemplate, HueConnect.BridgeIP, "srBM4Wmu7FgO4qHx0o6MNdibKXtOrpZurFioT17a", "lights", 1, "state"), data);
        }
    }
}
