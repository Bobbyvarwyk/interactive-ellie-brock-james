﻿using Rssdp;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhilipsHueBasicController
{
    class ColorSender
    {
        public const string LightsUrlTemplate = "http://{0}/api/{1}/{2}";
        public const string ControlLightUrlTemplate = "http://{0}/api/{1}/{2}/{3}/{4}";

        public string getRequestToBridge(string fullUrl)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullUrl);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
                return reader.ReadToEnd();
        }

        public void putRequestToBridge(string fullUri, string data, string contentType = "application/json", string method = "PUT")
        {
            using (var client = new System.Net.WebClient())
            {
                client.UploadData(fullUri, method, Encoding.UTF8.GetBytes(data));
            }

        }
    }
}
