//Check for Hue bridge in network
var hue = require("node-hue-api"), 
    timeout = 2000,
    HueApi = hue.api,
    lightState = hue.lightState;


var displayBridges = function(bridge) {
    console.log("Hue Bridges found: " + JSON.stringify(bridge));
}

hue.upnpSearch(timeout).then(displayBridges).done();

var displayResult = function(result) {
    console.log(JSON.stringify(result, null, 2));
}

var host = "192.168.137.142",
    username = "srBM4Wmu7FgO4qHx0o6MNdibKXtOrpZurFioT17a",
    api = new HueApi(host, username),
    state = lightState.create().on().hsl(356, 100, 41);

api.lights().then(displayResult).done();

//Control Lights

var displayLResult = function(result) {
    console.log(result);
};

var displayError = function(err) {
    console.error(err);
};

api.setLightState(1, state).then(displayLResult).fail(displayError).done();