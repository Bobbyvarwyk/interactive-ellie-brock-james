window.onload = function(){
    setTimeout(function(){
        init();
    }, 1000);
}

function sendYesHue() {
    sendToHue("PUT", 1, {
        "on": true,
        "hue": 56100,
        "bri": 254,
        "sat": 254,
        "alert": "lselect"
    });      
}

 function sendYesHueSong() {
    sendToHue("PUT", 1, {
        "on": true,
        "hue": 56100,
        "bri": 254,
        "sat": 254,
        "alert": "lselect"
    });
    playSong(true);         
}

function sendNoHue() {
    sendToHue("PUT", 1, {
        "on": true,
        "hue": 65280,
        "bri": 254,
        "sat": 254,
        "alert":"none"
    });        
}

 function sendNoHueSong() {
    sendToHue("PUT", 1, {
        "on": true,
        "hue": 65280,
        "bri": 254,
        "sat": 254,
        "alert":"none"
    });
    playSong(false);         
}

function Neutral() {
    sendToHue("PUT", 1, {
        "on": true,
        "hue": 9000,
        "bri": 254,
        "sat": 254,
        "alert":"none",
        "transitiontime":10
    });    
}

function init() {
    var body = document.getElementsByTagName('body')[0];
    var video = document.getElementById('video');
    var context = document.getElementById('canvas').getContext('2d');
    var tracker = new tracking.ObjectTracker('face');
    var bool = false;
    tracker.setInitialScale(4);
    tracker.setStepSize(1);
    tracker.setEdgesDensity(0.1);
    tracking.track(video, tracker, { camera: true });
    tracker.on('track', function(event) {
        context.drawImage(video, 0, 0, video.width, video.height);
        if (event.data.length !== 0) {

            event.data.forEach(function(rect) {
                context.strokeStyle = '#a64ceb';
                context.strokeRect(rect.x, rect.y, rect.width, rect.height);
                context.font = '11px Helvetica';
                context.fillStyle = "#fff";
                context.fillText('x: ' + rect.x + 'px', rect.x + rect.width + 5, rect.y + 11);
                context.fillText('y: ' + rect.y + 'px', rect.x + rect.width + 5, rect.y + 22);
            });
            bool = true;
        } else {
            bool = false;
        }
    });

    setInterval(function () {
        onTracked();
    }, 2000);
    onTracked();

    var firstSeen = false;

    function onTracked() {
        if(bool) {
            document.getElementById("settracked").innerHTML = "Yes";
        } else {
            document.getElementById("settracked").innerHTML = "No";
        }
    }

    /*function onTracked() {
        if (bool) {
            body.style.backgroundColor = "green";
            sendToHue("PUT", 1, {
                "on": true,
                "hue": 56100,
                "bri": 254,
                "sat": 254,
                "alert": "lselect"
            });
            playSong(true);
            
            if(firstSeen == false) {
                speakLine(true); 
                firstSeen = true;
            } else {
                console.log("Hoi");
            }
                         
        } else {
            body.style.backgroundColor = "red";
            sendToHue("PUT", 1, {
                "on": true,
                "hue": 65280,
                "bri": 254,
                "sat": 254,
                "alert":"1select"
            });
            playSong(false);  
        }
    } */
};