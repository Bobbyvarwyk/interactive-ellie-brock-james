var song = new Audio();
song.src = "songs/stop!.mp3";

var succesSong = new Audio();
succesSong.src = "songs/letsgetiton.mp3";

var speak = new Audio();
speak.src = "songs/Speak-brock.mp3"

function playSong(succes) {
    if(succes) {
        succesSong.play();
        song.pause();
        song.currentTime = 0;
    } else {
        song.play();
        succesSong.pause();
    }
}

function speakLine(succes) {
    speak.play();
}