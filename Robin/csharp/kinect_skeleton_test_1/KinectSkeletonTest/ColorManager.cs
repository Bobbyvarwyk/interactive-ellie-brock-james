﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace KinectSkeletonTest {

    class ColorManager {

        private static Color[] colors = new Color[]{
            Color.Red, Color.Orange, Color.Yellow, Color.Green, Color.Cyan, Color.LightBlue, Color.Blue, Color.DarkBlue, Color.Purple, Color.Pink, Color.White, Color.LightGray, Color.Gray, Color.DarkGray, Color.Black
        };

        public static Bitmap toBitmap(BitmapSource source) {
            Bitmap bmp = new Bitmap(source.PixelWidth, source.PixelHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            BitmapData data = bmp.LockBits(new System.Drawing.Rectangle(System.Drawing.Point.Empty, bmp.Size), ImageLockMode.WriteOnly, bmp.PixelFormat);
            source.CopyPixels(Int32Rect.Empty, data.Scan0, data.Height * data.Stride, data.Stride);
            bmp.UnlockBits(data);
            return bmp;
        }

        public static BitmapSource toBitmapSource(Bitmap bitmap) {
            BitmapData bitmapData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);

            BitmapSource bitmapSource = BitmapSource.Create(
                bitmapData.Width, bitmapData.Height,
                bitmap.HorizontalResolution, bitmap.VerticalResolution,
                System.Windows.Media.PixelFormats.Bgr32, null,
                bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);

            bitmap.UnlockBits(bitmapData);
            return bitmapSource;
        }

        public static Bitmap toBitmap(Color color, int width, int heigth) {
            Bitmap bitmap = new Bitmap(width, heigth);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < heigth; y++) {
                    bitmap.SetPixel(x, y, color);
                }
            }
            return bitmap;
        }

        public static Color avarage(Bitmap bitmap) {
            int alphaBucket = 0;
            int redBucket = 0;
            int greenBucket = 0;
            int blueBucket = 0;
            int pixelCount = 0;

            for (int y = 0; y < bitmap.Height; y++) {
                for (int x = 0; x < bitmap.Width; x++) {
                    Color c = bitmap.GetPixel(x, y);
                    pixelCount++;
                    alphaBucket += c.A;
                    redBucket += c.R;
                    greenBucket += c.G;
                    blueBucket += c.B;
                }
            }
            Color averageColor = Color.FromArgb(alphaBucket / pixelCount, redBucket / pixelCount, greenBucket / pixelCount, blueBucket / pixelCount);

            return averageColor;
        }

        public static Color compare(Color chosen_color) {
            double dbl_input_red = Convert.ToDouble(chosen_color.R);
            double dbl_input_green = Convert.ToDouble(chosen_color.G);
            double dbl_input_blue = Convert.ToDouble(chosen_color.B);
            double dbl_test_red, dbl_test_green, dbl_test_blue;
            double distance = 500.0;

            Color nearest_color = Color.Empty;
            foreach (Color color in colors) {
                dbl_test_red = Math.Pow(Convert.ToDouble(color.R) - dbl_input_red, 2.0);
                dbl_test_green = Math.Pow(Convert.ToDouble(color.G) - dbl_input_green, 2.0);
                dbl_test_blue = Math.Pow(Convert.ToDouble(color.B) - dbl_input_blue, 2.0);

                double temp = Math.Sqrt(dbl_test_blue + dbl_test_green + dbl_test_red);
                if (temp == 0.0) {
                    nearest_color = color;
                    break;
                } else if (temp < distance) {
                    distance = temp;
                    nearest_color = color;
                }
            }
            return nearest_color;
        }
    }
}
