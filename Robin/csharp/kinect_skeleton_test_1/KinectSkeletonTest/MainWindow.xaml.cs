﻿using System;
using System.Linq;
using System.Windows;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Kinect;
using System.Drawing.Imaging;
using System.Collections.Generic;

namespace KinectSkeletonTest {

    public partial class MainWindow : Window {
        
        KinectManager kinectManager;

        public MainWindow() {
            InitializeComponent();

            kinectManager = new KinectManager(video, face, color, status_color);
        }
       

        private void onLoaded(object sender, RoutedEventArgs e) {
            if (!kinectManager.init()) {
                MessageBox.Show("No Kinects found");
            }
        }

        private void onChangeStatus(bool tracked) {
            if (tracked) {
                status_bar.Text = "Status: Found a face";
            } else {
                status_bar.Text = "Status: Face lost...";
            }
        }

        private void onVideoRender() {

        }

        private void onClosed(object sender, System.ComponentModel.CancelEventArgs e) {

        }

        private void onClickRefresh(object sender, RoutedEventArgs e) {
            onLoaded(sender, e);
        }
    }
}
