﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Drawing;

namespace KinectSkeletonTest {

    class KinectManager {

        private KinectSensor sensor;
        Skeleton[] all_skeletons = new Skeleton[SKELETON_COUNTER];
        const int SKELETON_COUNTER = 6;
        
        BitmapSource source;
        Color selected_color;

        System.Windows.Controls.Image video_frame;
        System.Windows.Controls.Image found_frame;
        System.Windows.Controls.Image color_frame;
        System.Windows.Controls.TextBox status_block;

        public KinectManager(System.Windows.Controls.Image video, System.Windows.Controls.Image found, System.Windows.Controls.Image color, System.Windows.Controls.TextBox text) {
            video_frame = video;
            found_frame = found;
            color_frame = color;
            status_block = text;
        }


        public bool init() {
            if (KinectSensor.KinectSensors.Count > 0) {
                sensor = KinectSensor.KinectSensors[0];

                if (sensor.Status == KinectStatus.Connected) {
                    sensor.ColorStream.Enable();
                    sensor.DepthStream.Enable();
                    sensor.SkeletonStream.Enable();

                    sensor.DepthStream.Range = DepthRange.Near;
                    sensor.SkeletonStream.EnableTrackingInNearRange = true;
                    sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;

                    sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(onChangeSensorFrames);
                    sensor.Start();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        
        private void onChangeSensorFrames(object sender, AllFramesReadyEventArgs e) {
            using (ColorImageFrame color_frame = e.OpenColorImageFrame()) {
                if (color_frame == null) return;

                byte[] pixels = new byte[color_frame.PixelDataLength];
                color_frame.CopyPixelDataTo(pixels);

                int stride = color_frame.Width * 4;

                source = BitmapSource.Create(color_frame.Width, color_frame.Height, 96, 96, System.Windows.Media.PixelFormats.Bgr32, null, pixels, stride);
                video_frame.Source = source;
            }
            Skeleton me = null;
            getSkeleton(e, ref me);
            if (me == null) return;
            getCameraPoint(me, e);
        }

        private void getSkeleton(AllFramesReadyEventArgs e, ref Skeleton me) {
            using (SkeletonFrame skeleton_frame_data = e.OpenSkeletonFrame()) {
                if (skeleton_frame_data == null) return;

                skeleton_frame_data.CopySkeletonDataTo(all_skeletons);

                me = (from s in all_skeletons where s.TrackingState == SkeletonTrackingState.Tracked select s).FirstOrDefault();
            }
        }

        private void getCameraPoint(Skeleton me, AllFramesReadyEventArgs e) {
            using (DepthImageFrame depth = e.OpenDepthImageFrame()) {
                if (depth == null || sensor == null) return;

                DepthImagePoint depth_point = depth.MapFromSkeletonPoint(me.Joints[JointType.ShoulderCenter].Position);
                // ColorImagePoint color_point = depth.MapToColorImagePoint(depth_point.X, depth_point.Y, ColorImageFormat.RawBayerResolution640x480Fps30);
                //skull.SetBinding(Image.BindingGroupProperty, new Binding("Skull"));

                if (isOutOfBounds(depth_point.X, depth_point.Y)) {
                    found_frame.Source = null;
                    return;
                } else {
                    found_frame.Width = found_frame.Height = 200;
                    // overlay.Source = overlay_source;

                    double X = depth_point.X - found_frame.Width / 2 + 50;
                    double Y = depth_point.Y - found_frame.Height / 2 + 300;

                    // Canvas.SetLeft(overlay, X);
                    // Canvas.SetTop(overlay, Y);

                    // Clone a portion of the Bitmap object.
                    try {
                        Bitmap myBitmap = ColorManager.toBitmap(source);
                        Rectangle cloneRect = new Rectangle(Convert.ToInt32(X), Convert.ToInt32(Y), 100, 100);
                        PixelFormat format = myBitmap.PixelFormat;
                        Bitmap cloneBitmap = myBitmap.Clone(cloneRect, format);

                        found_frame.Source = ColorManager.toBitmapSource(cloneBitmap);

                        selected_color = ColorManager.avarage(cloneBitmap);
                        color_frame.Source = ColorManager.toBitmapSource(ColorManager.toBitmap(selected_color, 100, 100));
                        status_block.Text = ColorManager.compare(selected_color).ToString();
                        myBitmap = cloneBitmap = null;
                    } catch (Exception ex) {
                        Console.WriteLine(ex);

                    }
                }
            }
        }

        private bool isOutOfBounds(int X, int Y) {
            int max_x = (int)video_frame.Width - 10;
            int max_y = (int)video_frame.Height - 10;

            bool boolean = false;
            if (X < 10) {
                boolean = true;
            } else if (Y < 10) {
                boolean = true;
            } else if (X > max_x) {
                boolean = true;
            } else if (X > max_y) {
                boolean = true;
            } else {
                boolean = false;
            }

            return boolean;
        }
    }
}
