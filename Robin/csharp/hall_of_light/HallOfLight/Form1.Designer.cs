﻿namespace HallOfLight {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.video_frame = new System.Windows.Forms.PictureBox();
            this.zoom_frame = new System.Windows.Forms.PictureBox();
            this.color_frame = new System.Windows.Forms.PictureBox();
            this.color_status = new System.Windows.Forms.Label();
            this.button_red = new System.Windows.Forms.Button();
            this.button_orange = new System.Windows.Forms.Button();
            this.button_yellow = new System.Windows.Forms.Button();
            this.button_lime = new System.Windows.Forms.Button();
            this.button_green = new System.Windows.Forms.Button();
            this.button_cyan = new System.Windows.Forms.Button();
            this.button_blue = new System.Windows.Forms.Button();
            this.button_purple = new System.Windows.Forms.Button();
            this.button_magenta_dark = new System.Windows.Forms.Button();
            this.button_magenta = new System.Windows.Forms.Button();
            this.button_pink = new System.Windows.Forms.Button();
            this.button_pink_light = new System.Windows.Forms.Button();
            this.button_white = new System.Windows.Forms.Button();
            this.button_grey_light = new System.Windows.Forms.Button();
            this.button_grey = new System.Windows.Forms.Button();
            this.button_black = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.video_frame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoom_frame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.color_frame)).BeginInit();
            this.SuspendLayout();
            // 
            // video_frame
            // 
            this.video_frame.Location = new System.Drawing.Point(13, 13);
            this.video_frame.Name = "video_frame";
            this.video_frame.Size = new System.Drawing.Size(640, 480);
            this.video_frame.TabIndex = 0;
            this.video_frame.TabStop = false;
            // 
            // zoom_frame
            // 
            this.zoom_frame.Location = new System.Drawing.Point(13, 500);
            this.zoom_frame.Name = "zoom_frame";
            this.zoom_frame.Size = new System.Drawing.Size(100, 100);
            this.zoom_frame.TabIndex = 1;
            this.zoom_frame.TabStop = false;
            // 
            // color_frame
            // 
            this.color_frame.Location = new System.Drawing.Point(119, 499);
            this.color_frame.Name = "color_frame";
            this.color_frame.Size = new System.Drawing.Size(100, 100);
            this.color_frame.TabIndex = 2;
            this.color_frame.TabStop = false;
            // 
            // color_status
            // 
            this.color_status.AutoSize = true;
            this.color_status.Location = new System.Drawing.Point(225, 499);
            this.color_status.Name = "color_status";
            this.color_status.Size = new System.Drawing.Size(64, 13);
            this.color_status.TabIndex = 3;
            this.color_status.Text = "color_status";
            // 
            // button_red
            // 
            this.button_red.Location = new System.Drawing.Point(660, 13);
            this.button_red.Name = "button_red";
            this.button_red.Size = new System.Drawing.Size(75, 75);
            this.button_red.TabIndex = 4;
            this.button_red.Text = "button1";
            this.button_red.UseVisualStyleBackColor = true;
            // 
            // button_orange
            // 
            this.button_orange.Location = new System.Drawing.Point(741, 13);
            this.button_orange.Name = "button_orange";
            this.button_orange.Size = new System.Drawing.Size(75, 75);
            this.button_orange.TabIndex = 5;
            this.button_orange.Text = "button1";
            this.button_orange.UseVisualStyleBackColor = true;
            // 
            // button_yellow
            // 
            this.button_yellow.Location = new System.Drawing.Point(822, 12);
            this.button_yellow.Name = "button_yellow";
            this.button_yellow.Size = new System.Drawing.Size(75, 75);
            this.button_yellow.TabIndex = 6;
            this.button_yellow.Text = "button1";
            this.button_yellow.UseVisualStyleBackColor = true;
            // 
            // button_lime
            // 
            this.button_lime.Location = new System.Drawing.Point(903, 12);
            this.button_lime.Name = "button_lime";
            this.button_lime.Size = new System.Drawing.Size(75, 75);
            this.button_lime.TabIndex = 7;
            this.button_lime.Text = "button1";
            this.button_lime.UseVisualStyleBackColor = true;
            // 
            // button_green
            // 
            this.button_green.Location = new System.Drawing.Point(660, 94);
            this.button_green.Name = "button_green";
            this.button_green.Size = new System.Drawing.Size(75, 75);
            this.button_green.TabIndex = 8;
            this.button_green.Text = "button1";
            this.button_green.UseVisualStyleBackColor = true;
            // 
            // button_cyan
            // 
            this.button_cyan.Location = new System.Drawing.Point(741, 94);
            this.button_cyan.Name = "button_cyan";
            this.button_cyan.Size = new System.Drawing.Size(75, 75);
            this.button_cyan.TabIndex = 9;
            this.button_cyan.Text = "button1";
            this.button_cyan.UseVisualStyleBackColor = true;
            // 
            // button_blue
            // 
            this.button_blue.Location = new System.Drawing.Point(822, 94);
            this.button_blue.Name = "button_blue";
            this.button_blue.Size = new System.Drawing.Size(75, 75);
            this.button_blue.TabIndex = 10;
            this.button_blue.Text = "button1";
            this.button_blue.UseVisualStyleBackColor = true;
            // 
            // button_purple
            // 
            this.button_purple.Location = new System.Drawing.Point(903, 93);
            this.button_purple.Name = "button_purple";
            this.button_purple.Size = new System.Drawing.Size(75, 75);
            this.button_purple.TabIndex = 11;
            this.button_purple.Text = "button1";
            this.button_purple.UseVisualStyleBackColor = true;
            // 
            // button_magenta_dark
            // 
            this.button_magenta_dark.Location = new System.Drawing.Point(660, 175);
            this.button_magenta_dark.Name = "button_magenta_dark";
            this.button_magenta_dark.Size = new System.Drawing.Size(75, 75);
            this.button_magenta_dark.TabIndex = 12;
            this.button_magenta_dark.Text = "button1";
            this.button_magenta_dark.UseVisualStyleBackColor = true;
            // 
            // button_magenta
            // 
            this.button_magenta.Location = new System.Drawing.Point(741, 175);
            this.button_magenta.Name = "button_magenta";
            this.button_magenta.Size = new System.Drawing.Size(75, 75);
            this.button_magenta.TabIndex = 13;
            this.button_magenta.Text = "button1";
            this.button_magenta.UseVisualStyleBackColor = true;
            // 
            // button_pink
            // 
            this.button_pink.Location = new System.Drawing.Point(822, 175);
            this.button_pink.Name = "button_pink";
            this.button_pink.Size = new System.Drawing.Size(75, 75);
            this.button_pink.TabIndex = 14;
            this.button_pink.Text = "button1";
            this.button_pink.UseVisualStyleBackColor = true;
            // 
            // button_pink_light
            // 
            this.button_pink_light.Location = new System.Drawing.Point(903, 175);
            this.button_pink_light.Name = "button_pink_light";
            this.button_pink_light.Size = new System.Drawing.Size(75, 75);
            this.button_pink_light.TabIndex = 15;
            this.button_pink_light.Text = "button1";
            this.button_pink_light.UseVisualStyleBackColor = true;
            // 
            // button_white
            // 
            this.button_white.Location = new System.Drawing.Point(660, 256);
            this.button_white.Name = "button_white";
            this.button_white.Size = new System.Drawing.Size(75, 75);
            this.button_white.TabIndex = 16;
            this.button_white.Text = "button1";
            this.button_white.UseVisualStyleBackColor = true;
            // 
            // button_grey_light
            // 
            this.button_grey_light.Location = new System.Drawing.Point(741, 256);
            this.button_grey_light.Name = "button_grey_light";
            this.button_grey_light.Size = new System.Drawing.Size(75, 75);
            this.button_grey_light.TabIndex = 17;
            this.button_grey_light.Text = "button1";
            this.button_grey_light.UseVisualStyleBackColor = true;
            // 
            // button_grey
            // 
            this.button_grey.Location = new System.Drawing.Point(822, 256);
            this.button_grey.Name = "button_grey";
            this.button_grey.Size = new System.Drawing.Size(75, 75);
            this.button_grey.TabIndex = 18;
            this.button_grey.Text = "button1";
            this.button_grey.UseVisualStyleBackColor = true;
            // 
            // button_black
            // 
            this.button_black.Location = new System.Drawing.Point(903, 256);
            this.button_black.Name = "button_black";
            this.button_black.Size = new System.Drawing.Size(75, 75);
            this.button_black.TabIndex = 19;
            this.button_black.Text = "button1";
            this.button_black.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 661);
            this.Controls.Add(this.button_black);
            this.Controls.Add(this.button_grey);
            this.Controls.Add(this.button_grey_light);
            this.Controls.Add(this.button_white);
            this.Controls.Add(this.button_pink_light);
            this.Controls.Add(this.button_pink);
            this.Controls.Add(this.button_magenta);
            this.Controls.Add(this.button_magenta_dark);
            this.Controls.Add(this.button_purple);
            this.Controls.Add(this.button_blue);
            this.Controls.Add(this.button_cyan);
            this.Controls.Add(this.button_green);
            this.Controls.Add(this.button_lime);
            this.Controls.Add(this.button_yellow);
            this.Controls.Add(this.button_orange);
            this.Controls.Add(this.button_red);
            this.Controls.Add(this.color_status);
            this.Controls.Add(this.color_frame);
            this.Controls.Add(this.zoom_frame);
            this.Controls.Add(this.video_frame);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.video_frame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoom_frame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.color_frame)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox video_frame;
        private System.Windows.Forms.PictureBox zoom_frame;
        private System.Windows.Forms.PictureBox color_frame;
        private System.Windows.Forms.Label color_status;
        private System.Windows.Forms.Button button_red;
        private System.Windows.Forms.Button button_orange;
        private System.Windows.Forms.Button button_yellow;
        private System.Windows.Forms.Button button_lime;
        private System.Windows.Forms.Button button_green;
        private System.Windows.Forms.Button button_cyan;
        private System.Windows.Forms.Button button_blue;
        private System.Windows.Forms.Button button_purple;
        private System.Windows.Forms.Button button_magenta_dark;
        private System.Windows.Forms.Button button_magenta;
        private System.Windows.Forms.Button button_pink;
        private System.Windows.Forms.Button button_pink_light;
        private System.Windows.Forms.Button button_white;
        private System.Windows.Forms.Button button_grey_light;
        private System.Windows.Forms.Button button_grey;
        private System.Windows.Forms.Button button_black;
    }
}

