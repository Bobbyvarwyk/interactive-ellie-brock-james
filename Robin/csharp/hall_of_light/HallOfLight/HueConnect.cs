﻿using Rssdp;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HallOfLight
{
    class HueConnect
    {
        public static string BridgeIP;

        private const string APIAddressTemplate = "http://{0}/api";
        private const string BodyTemplate = "{{\"devicetype\":\"{0}\"}}";

        public bool findBridgeIP()
        {
            bool found = false;
            SsdpDeviceLocator devicelocator = new SsdpDeviceLocator(getLocalIPAdres().ToString());
            var foundDevices = devicelocator.SearchAsync().Result.ToList();

            foreach (var device in foundDevices)
            {
                if (device.ResponseHeaders.ToString().Contains("IpBridge"))
                {
                    BridgeIP = device.DescriptionLocation.Host;
                    found = true;
                }
                else
                {
                    BridgeIP = "Not Found";
                }
            }
            return found;
        }

        private IPAddress getLocalIPAdres()
        {

            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable()) return null;

            return Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }
    }
}
