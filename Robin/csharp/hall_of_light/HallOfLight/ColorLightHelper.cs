﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Drawing;

namespace HallOfLight
{
    public partial class ColorLightHelper
    {
        [JsonProperty("xy")]
        public float[] xy { get; set; }
        [JsonProperty("transitiontime")]
        public int transitiontime { get; set; }

        private ColorLightHelper(float[] xy_code, int transition)
        {
            xy = xy_code;
            transitiontime = transition;
        }

        public static void setColor(Color color) {
            ColorLightHelper help = new ColorLightHelper(ColorConverter.convert(color), 10);
            string data = JsonConvert.SerializeObject(help);
            ColorLight controls = new ColorLight();
            controls.putRequestToBridge(string.Format(ColorLight.ControlLightUrlTemplate, HueConnect.BridgeIP, "srBM4Wmu7FgO4qHx0o6MNdibKXtOrpZurFioT17a", "lights", 1, "state"), data);
        }
    }
}
