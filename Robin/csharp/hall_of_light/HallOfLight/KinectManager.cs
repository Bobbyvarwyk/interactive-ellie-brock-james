﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Runtime.InteropServices;

namespace HallOfLight {

    class KinectManager {

        bool LOOK_CLOSE = false;
        SkeletonTrackingMode MODE;
        int LOOK;

        private KinectSensor sensor;
        Skeleton[] all_skeletons = new Skeleton[SKELETON_COUNTER];
        const int SKELETON_COUNTER = 6;

        Bitmap source;

        Color selected_color;

        Form1 form;
        PictureBox video_frame;
        PictureBox found_frame;
        PictureBox color_frame;
        Label status_block;

        public KinectManager(Form1 form, PictureBox video, PictureBox found, PictureBox color, Label text) {
            this.form = form;
            video_frame = video;
            video_frame.BackColor = Color.Blue;
            found_frame = found;
            found_frame.BackColor = Color.Yellow;
            color_frame = color;
            color_frame.BackColor = Color.Red;
            status_block = text;
        }

        public Color getColor() { return selected_color;  }

        public bool init() {

            if (LOOK_CLOSE) {
                MODE = SkeletonTrackingMode.Seated;
                LOOK = 300;
            } else {
                MODE = SkeletonTrackingMode.Default;
                LOOK = 100;
            }


            if (KinectSensor.KinectSensors.Count > 0) {
                sensor = KinectSensor.KinectSensors[0];

                if (sensor.Status == KinectStatus.Connected) {
                    sensor.ColorStream.Enable();
                    sensor.DepthStream.Enable();
                    sensor.SkeletonStream.Enable();

                    sensor.DepthStream.Range = DepthRange.Near;
                    sensor.SkeletonStream.EnableTrackingInNearRange = true;
                    sensor.SkeletonStream.TrackingMode = MODE;

                    sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(onChangeSensorFrames);
                    sensor.Start();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        private void onChangeSensorFrames(object sender, AllFramesReadyEventArgs e) {
            using (ColorImageFrame color_frame = e.OpenColorImageFrame()) {
                if (color_frame == null) return;

                byte[] pixels = new byte[color_frame.PixelDataLength];
                color_frame.CopyPixelDataTo(pixels);

                int stride = color_frame.Width * 4;

                //source = ColorManager.toBitmap(BitmapSource.Create(color_frame.Width, color_frame.Height, 96, 96, System.Windows.Media.PixelFormats.Bgr32, null, pixels, stride));

                source = ColorImageFrameToBitmap(color_frame);
                video_frame.Image = source;
            }
            Skeleton me = null;
            getSkeleton(e, ref me);
            if (me == null) return;
            getCameraPoint(me, e);
        }

        private static Bitmap ColorImageFrameToBitmap(ColorImageFrame colorFrame) {
            byte[] pixelBuffer = new byte[colorFrame.PixelDataLength];
            colorFrame.CopyPixelDataTo(pixelBuffer);


            Bitmap bitmapFrame = new Bitmap(colorFrame.Width, colorFrame.Height, PixelFormat.Format32bppRgb);


            BitmapData bitmapData = bitmapFrame.LockBits(new Rectangle(0, 0, colorFrame.Width, colorFrame.Height),
            ImageLockMode.WriteOnly, bitmapFrame.PixelFormat);


            IntPtr intPointer = bitmapData.Scan0;
            Marshal.Copy(pixelBuffer, 0, intPointer, colorFrame.PixelDataLength);
            
            bitmapFrame.UnlockBits(bitmapData);

            return bitmapFrame;
        }

        private void getSkeleton(AllFramesReadyEventArgs e, ref Skeleton me) {
            using (SkeletonFrame skeleton_frame_data = e.OpenSkeletonFrame()) {
                if (skeleton_frame_data == null) return;

                skeleton_frame_data.CopySkeletonDataTo(all_skeletons);

                me = (from s in all_skeletons where s.TrackingState == SkeletonTrackingState.Tracked select s).FirstOrDefault();
            }
        }

        private void getCameraPoint(Skeleton me, AllFramesReadyEventArgs e) {
            using (DepthImageFrame depth = e.OpenDepthImageFrame()) {
                if (depth == null || sensor == null) return;

                DepthImagePoint depth_point = depth.MapFromSkeletonPoint(me.Joints[JointType.ShoulderCenter].Position);
                // ColorImagePoint color_point = depth.MapToColorImagePoint(depth_point.X, depth_point.Y, ColorImageFormat.RawBayerResolution640x480Fps30);
                //skull.SetBinding(Image.BindingGroupProperty, new Binding("Skull"));

                if (isOutOfBounds(depth_point.X, depth_point.Y)) {
                    found_frame.Image = null;
                    return;
                } else {
                    // overlay.Source = overlay_source;

                    double X = depth_point.X - found_frame.Width / 2 + 50;
                    double Y = depth_point.Y - found_frame.Height / 2 + LOOK;

                    // Canvas.SetLeft(overlay, X);
                    // Canvas.SetTop(overlay, Y);

                    // Clone a portion of the Bitmap object.
                    try {
                        Bitmap myBitmap = source;
                        Rectangle cloneRect = new Rectangle(Convert.ToInt32(X), Convert.ToInt32(Y), 50, 50);
                        Bitmap cloneBitmap = myBitmap.Clone(cloneRect, myBitmap.PixelFormat);
                        selected_color = ColorManager.avarage(cloneBitmap);
                        form.onFramesChanged();

                        if (found_frame != null) {
                            found_frame.Image = cloneBitmap;
                        }
                        if (color_frame != null) {
                            color_frame.Image = ColorManager.toBitmap(selected_color, 100, 100);
                        }
                        if (status_block != null) {
                            status_block.Text = ColorManager.compare(selected_color).ToString();
                        }
                        myBitmap = cloneBitmap = null;
                    } catch (Exception ex) {
                        Console.WriteLine(ex);

                    }
                }
            }
        }

        private bool isOutOfBounds(int X, int Y) {
            int max_x = (int)video_frame.Width - 10;
            int max_y = (int)video_frame.Height - 10;

            bool boolean = false;
            if (X < 10) {
                boolean = true;
            } else if (Y < 10) {
                boolean = true;
            } else if (X > max_x) {
                boolean = true;
            } else if (X > max_y) {
                boolean = true;
            } else {
                boolean = false;
            }

            return boolean;
        }
    }
}
