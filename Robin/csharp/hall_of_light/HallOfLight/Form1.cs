﻿using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace HallOfLight {

    public partial class Form1 : Form {

        HueConnect connectHue;
        KinectManager kinectManager;
        Color current_color;

        public Form1() {
            InitializeComponent();
            connectHue = new HueConnect();
            kinectManager = new KinectManager(this, video_frame, zoom_frame, color_frame, color_status);
            this.Load += onWindowLoad;
        }

        private void onWindowLoad(object sender, System.EventArgs e) {
            if (kinectManager.init()) {
                if (connectHue.findBridgeIP()) {
                    initButtons();
                } else {
                    MessageBox.Show("No bridge found...");
                }
            } else {
                MessageBox.Show("No Kinects found...");
            }
        }

        private void initButtons() {
            setButton(button_red, Color.Red, Color.White);
            setButton(button_orange, Color.Orange, Color.White);
            setButton(button_yellow, Color.Yellow, Color.White);
            setButton(button_lime, Color.LimeGreen, Color.White);

            setButton(button_green, Color.Green, Color.White);
            setButton(button_cyan, Color.Cyan, Color.White);
            setButton(button_blue, Color.Blue, Color.White);
            setButton(button_purple,  Color.Purple, Color.White);

            setButton(button_magenta_dark, Color.DarkMagenta, Color.White);
            setButton(button_magenta, Color.Magenta, Color.White);
            setButton(button_pink, Color.Pink, Color.White);
            setButton(button_pink_light, Color.LightPink, Color.Black);

            setButton(button_white, Color.White, Color.Black);
            setButton(button_grey_light, Color.LightGray, Color.Black);
            setButton(button_grey, Color.Gray, Color.White);
            setButton(button_black, Color.Black, Color.White);
        }

        private void setButton(Button button, Color back, Color front) {
            button.Text = back.ToString();
            button.BackColor = back;
            button.ForeColor = front;
            button.Click += onColorButtonClick;
        }

        private void onColorButtonClick(object sender, EventArgs e) {
            Button button = (Button)sender;
            ColorLightHelper.setColor(button.BackColor);
        }

        public void onFramesChanged() {
            Color color = kinectManager.getColor();
            if (current_color != color) {
                current_color = color;
                ColorLightHelper.setColor(color);
            }
        }
    }
}
