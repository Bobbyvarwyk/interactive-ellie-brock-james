﻿using System;
using System.Linq;
using System.Windows;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Kinect;
using System.Drawing.Imaging;
using System.Collections.Generic;

namespace KinectSkeletonTest {

    public partial class MainWindow : Window {

        enum Character { AdolfHitler, EwanMcGregor, HarrisonFord, NicolasCage, ChuckNorris, RowanAdkinson, RobGeus }

        Character head = Character.HarrisonFord;
        BitmapImage overlay_source;
        BitmapSource source;
        System.Drawing.Color selected_color;

        KinectSensor sensor;
        Skeleton[] all_skeletons = new Skeleton[SKELETON_COUNTER];
        const int SKELETON_COUNTER = 6;

        static List<System.Drawing.Color> colors = new List<System.Drawing.Color>();

        public MainWindow() {
            InitializeComponent();
            setOverlay(head);

            colors.Add(System.Drawing.Color.Red);
            colors.Add(System.Drawing.Color.Orange);
            colors.Add(System.Drawing.Color.Yellow);
            colors.Add(System.Drawing.Color.Green);
            colors.Add(System.Drawing.Color.Blue);
            colors.Add(System.Drawing.Color.Purple);
            colors.Add(System.Drawing.Color.Black);
            colors.Add(System.Drawing.Color.Gray);
        }

        private void setOverlay(Character character) {
            string url;
            switch (character) {
                case Character.AdolfHitler:
                    url = "https://mbtskoudsalg.com/images/transparent-hitler-head-2.png";
                    break;
                case Character.ChuckNorris:
                    url = "http://pngimg.com/uploads/chuck_norris/chuck_norris_PNG27.png";
                    break;
                default:
                case Character.EwanMcGregor:
                    url = "https://i.imgur.com/N3tvt5P.jpg";
                    break;
                case Character.HarrisonFord:
                    url = "https://mbtskoudsalg.com/images/harrison-ford-png-7.png";
                    break;
                case Character.NicolasCage:
                    url = "https://mbtskoudsalg.com/images/harrison-ford-png-7.png";
                    break;
                case Character.RobGeus:
                    url = "https://i.imgur.com/O0dJpGM.png";
                    break;
                case Character.RowanAdkinson:
                    url = "http://pngimg.com/uploads/mr_bean/mr_bean_PNG16.png";
                    break;
            }
            overlay_source = new BitmapImage(new Uri(url));
        }

        private void onLoaded(object sender, RoutedEventArgs e) {
            initKinects();
        }

        private void initKinects() {
            if (KinectSensor.KinectSensors.Count > 0) {
                sensor = KinectSensor.KinectSensors[0];

                if (sensor.Status == KinectStatus.Connected) {
                    sensor.ColorStream.Enable();
                    sensor.DepthStream.Enable();
                    sensor.SkeletonStream.Enable();

                    sensor.DepthStream.Range = DepthRange.Near;
                    sensor.SkeletonStream.EnableTrackingInNearRange = true;
                    sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;

                    sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(onChangeSensorFrames);
                    sensor.Start();
                }
            } else {
                MessageBox.Show("No Kinects found");
            }
        }

        void onChangeSensorFrames(object sender, AllFramesReadyEventArgs e) {
            using (ColorImageFrame color_frame = e.OpenColorImageFrame()) {
                if (color_frame == null) return;

                byte[] pixels = new byte[color_frame.PixelDataLength];
                color_frame.CopyPixelDataTo(pixels);

                int stride = color_frame.Width * 4;

                source = BitmapSource.Create(color_frame.Width, color_frame.Height, 96, 96, PixelFormats.Bgr32, null, pixels, stride);
                video.Source = source;
            }
            Skeleton me = null;
            getSkeleton(e, ref me);
            if (me == null) return;
            getCameraPoint(me, e);
        }

        private void getSkeleton(AllFramesReadyEventArgs e, ref Skeleton me) {
            using (SkeletonFrame skeleton_frame_data = e.OpenSkeletonFrame()) {
                if (skeleton_frame_data == null) return;

                skeleton_frame_data.CopySkeletonDataTo(all_skeletons);

                me = (from s in all_skeletons where s.TrackingState == SkeletonTrackingState.Tracked select s).FirstOrDefault();
            }
        }

        private void getCameraPoint(Skeleton me, AllFramesReadyEventArgs e) {
            using (DepthImageFrame depth = e.OpenDepthImageFrame()) {
                if (depth == null || sensor == null) return;

                DepthImagePoint depth_point = depth.MapFromSkeletonPoint(me.Joints[JointType.ShoulderCenter].Position);
                // ColorImagePoint color_point = depth.MapToColorImagePoint(depth_point.X, depth_point.Y, ColorImageFormat.RawBayerResolution640x480Fps30);
                //skull.SetBinding(Image.BindingGroupProperty, new Binding("Skull"));

                if (isOutOfBounds(depth_point.X, depth_point.Y)) {
                    overlay.Source = null;
                    return;
                } else {
                    overlay.Width = overlay.Height = 200;
                    // overlay.Source = overlay_source;

                    double X = depth_point.X - overlay.Width / 2 + 100;
                    double Y = depth_point.Y - overlay.Height / 2 + 200;

                    // Canvas.SetLeft(overlay, X);
                    // Canvas.SetTop(overlay, Y);
                    
                    // Clone a portion of the Bitmap object.
                    try {
                        Bitmap myBitmap = convertSourceToBitmap(source);
                        System.Drawing.Rectangle cloneRect = new System.Drawing.Rectangle(Convert.ToInt32(X), Convert.ToInt32(Y), 100, 100);
                        System.Drawing.Imaging.PixelFormat format = myBitmap.PixelFormat;
                        Bitmap cloneBitmap = myBitmap.Clone(cloneRect, format);
                        
                        face.Source = convertBitmapToSource(cloneBitmap);

                        selected_color = getAvarageColorFromBitmap(cloneBitmap);
                        color.Source = convertBitmapToSource(createBitmapFromColor(selected_color, 100, 100));
                        status_color.Text = "Color nearest to: " + GetNearestColorLabelIndex(selected_color, colors); ;
                        myBitmap = cloneBitmap = null;
                    } catch (Exception ex) {
                        Console.WriteLine(ex);

                    }
                }
                onChangeStatus(!isOutOfBounds(depth_point.X, depth_point.Y));
            }
        }

        Bitmap convertSourceToBitmap(BitmapSource source) {
            Bitmap bmp = new Bitmap(source.PixelWidth, source.PixelHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            BitmapData data = bmp.LockBits(new System.Drawing.Rectangle(System.Drawing.Point.Empty, bmp.Size), ImageLockMode.WriteOnly, bmp.PixelFormat);
            source.CopyPixels(Int32Rect.Empty, data.Scan0, data.Height * data.Stride, data.Stride);
            bmp.UnlockBits(data);
            return bmp;
        }

        public BitmapSource convertBitmapToSource(Bitmap bitmap) {
            BitmapData bitmapData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);

            BitmapSource bitmapSource = BitmapSource.Create(
                bitmapData.Width, bitmapData.Height,
                bitmap.HorizontalResolution, bitmap.VerticalResolution,
                PixelFormats.Bgr32, null,
                bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);

            bitmap.UnlockBits(bitmapData);
            return bitmapSource;
        }

        public Bitmap createBitmapFromColor(System.Drawing.Color color, int width, int heigth) {
            Bitmap bitmap = new Bitmap(width, heigth);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < heigth; y++) {
                    bitmap.SetPixel(x, y, color);
                }
            }
            return bitmap;
        }

        public System.Drawing.Color getAvarageColorFromBitmap(Bitmap bitmap) {
            int alphaBucket = 0;
            int redBucket = 0;
            int greenBucket = 0;
            int blueBucket = 0;
            int pixelCount = 0;

            for (int y = 0; y < bitmap.Height; y++) {
                for (int x = 0; x < bitmap.Width; x++) {
                    System.Drawing.Color c = bitmap.GetPixel(x, y);
                    pixelCount++;
                    alphaBucket += c.A;
                    redBucket += c.R;
                    greenBucket += c.G;
                    blueBucket += c.B;
                }
            }
            System.Drawing.Color averageColor = System.Drawing.Color.FromArgb(alphaBucket / pixelCount, redBucket / pixelCount, greenBucket / pixelCount, blueBucket / pixelCount);

            return averageColor;
        }

        public static System.Drawing.Color GetNearestColorLabelIndex(System.Drawing.Color chosen_color, List<System.Drawing.Color> LabelColorsHSB) {
            double dbl_input_red = Convert.ToDouble(chosen_color.R);
            double dbl_input_green = Convert.ToDouble(chosen_color.G);
            double dbl_input_blue = Convert.ToDouble(chosen_color.B);
            double dbl_test_red, dbl_test_green, dbl_test_blue;
            double distance = 500.0;

            System.Drawing.Color nearest_color = System.Drawing.Color.Empty;
            foreach (System.Drawing.Color color in LabelColorsHSB) {
                dbl_test_red = Math.Pow(Convert.ToDouble(color.R) - dbl_input_red, 2.0);
                dbl_test_green = Math.Pow(Convert.ToDouble(color.G) - dbl_input_green, 2.0);
                dbl_test_blue = Math.Pow(Convert.ToDouble(color.B) - dbl_input_blue, 2.0);

                double temp = Math.Sqrt(dbl_test_blue + dbl_test_green + dbl_test_red);
                if (temp == 0.0) {
                    nearest_color = color;
                    break;
                } else if (temp < distance) {
                    distance = temp;
                    nearest_color = color;
                }
            }
            return nearest_color;
        }

        private bool isOutOfBounds(int X, int Y) {
            int max_x = (int) video.Width - 10;
            int max_y = (int) video.Height - 10;

            bool boolean = false;
            if (X < 10) {
                boolean = true;
            } else if(Y < 10) {
                boolean = true;
            } else if (X > max_x) {
                boolean = true;
            } else if(X > max_y) {
                boolean = true;
            } else {
                boolean = false;
            }

            return boolean;
        }

        private void onChangeStatus(bool tracked) {
            if (tracked) {
                status_bar.Text = "Status: Found a face";
            } else {
                status_bar.Text = "Status: Face lost...";
            }
        }

        private void onClosed(object sender, System.ComponentModel.CancelEventArgs e) {

        }

        private void onClickRefresh(object sender, RoutedEventArgs e) {
            initKinects();
        }
    }
}
