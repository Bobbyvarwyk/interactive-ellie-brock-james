﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace KinectSkeletonTest {

    class KinectManager {

        private KinectSensor sensor;
        Skeleton[] all_skeletons = new Skeleton[SKELETON_COUNTER];
        const int SKELETON_COUNTER = 6;

        BitmapImage overlay_source;
        BitmapSource source;
        System.Drawing.Color selected_color;


        public bool initKinects() {
            if (KinectSensor.KinectSensors.Count > 0) {
                sensor = KinectSensor.KinectSensors[0];

                if (sensor.Status == KinectStatus.Connected) {
                    sensor.ColorStream.Enable();
                    sensor.DepthStream.Enable();
                    sensor.SkeletonStream.Enable();

                    sensor.DepthStream.Range = DepthRange.Near;
                    sensor.SkeletonStream.EnableTrackingInNearRange = true;
                    sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;

                    sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(onChangeSensorFrames);
                    sensor.Start();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        
        private void onChangeSensorFrames(object sender, AllFramesReadyEventArgs e) {
            using (ColorImageFrame color_frame = e.OpenColorImageFrame()) {
                if (color_frame == null) return;

                byte[] pixels = new byte[color_frame.PixelDataLength];
                color_frame.CopyPixelDataTo(pixels);

                int stride = color_frame.Width * 4;

                source = BitmapSource.Create(color_frame.Width, color_frame.Height, 96, 96, PixelFormats.Bgr32, null, pixels, stride);
                video.Source = source;
            }
            Skeleton me = null;
            getSkeleton(e, ref me);
            if (me == null) return;
            getCameraPoint(me, e);
        }

    }
}
