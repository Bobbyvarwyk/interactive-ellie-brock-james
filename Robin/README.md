# Intro
Dit is de map met Proof of Concepts van Robin van Uden.
Deze PoC's hebben te maken met James the Welcoming lamp en Brock the Love lamp.

# Overzicht
Javascript opdrachten staan in:
```
    \Robin\
        \js\
            - camera_js\
            - philips_hue_try
            - spotify_js
        \csharp\
            - hall_of_light\
            - kinect_skeleton_test_1
            - kinect_skeleton_test_2
            - kinect_tshirt_color_test_1
```

### camera_js: 
In dit Proof of Concept heb ik gebruik gemaakt van de webcam van mijn laptop als alternatief voor de Kinect.
Ik vooral gekeken hoe de camera werkt met Js en heb ik ook andere libraries gebruikt voor het tracken van een persoon. 
Hiervoor heb ik tracking.js gebruikt. Iets anders is er eigenlijk niet te vinden zonder node.js te gebruiken.
Tracking.js is wel goed te gebruiken om een gezicht te tracken maar helaas door tijdgebruik hebben we hier niet zoveel mee gedaan.

### spotify_js:
In dit Proof of Concept heb ik gebruik gemaakt van de Spotify API om zo muziek af te spelen via JS. 
Echter kwam ik tijdens het realiseren erachter dat Spotify heel wat functies beperkt heeft voor de 
veiligheid en is het eigenlijk alleen maar mogelijk om het huidige nummer te zien maar kun je niks beinvloeden.

### philips_hue:
In dit Proof of Concept heb ik gebruik gemaakt van de Philips Hue API alleen kon ik hier niet veel mee doen 
omdat mijn laptop en de Hue Bridge geen connectie konden maken… (lag aan mijn laptop). 
Hiermee is Bob verder gegaan maar ik wilde het toch mee geven.
